#pragma once

#include <glm/glm.hpp>
#include <GL/glew.h>
#include <string>

class Shader {
public:
  void create(std::string srcvertexShader, std::string srcfragShader);
  void use();
  void setMat4(const std::string &name, const glm::mat4 &mat);
  void setInt(const std::string &name, const int num);
  void destroy();

private:
  int shaderProgram;
};
