#pragma once

#include <GLFW/glfw3.h>
#include <map>
#include <memory>
#include <utility>

void keyCallb(GLFWwindow *window, int key, int, int action, int mods);
void scrollCallb(GLFWwindow *window, double xoffset, double yoffset);
void cursorPosCallb(GLFWwindow *window, double xpos, double ypos);
void mouseClickCallb(GLFWwindow *window, int button, int action, int mods);

class Input {

public:
  void setCallback(GLFWwindow *window) {
    glfwSetKeyCallback(window, keyCallb);
    glfwSetScrollCallback(window, scrollCallb);
    glfwSetCursorPosCallback(window, cursorPosCallb);
    glfwSetMouseButtonCallback(window, mouseClickCallb);
  }

  bool getKey(int key) const { return keyMap[key]; }
  bool getKeyOnce(int key);

  bool getMouseClick(int mouseButton) {
    if(mouseButton == GLFW_MOUSE_BUTTON_LEFT) {
      lastMouseClick = {mouseButton, true};
      return mouseClick.first;
    }
    if(mouseButton == GLFW_MOUSE_BUTTON_RIGHT) {
      lastMouseClick = {mouseButton, true};
      return mouseClick.second;
    }
    return false;
  }

  bool getMouseClickOnce(int mouseButton) {
  if (Input::lastMouseClick.first == mouseButton && Input::getMouseClick(mouseButton)) {
    Input::lastMouseClick = {-1, false};
    return true;
  }
  return false;
  }

  std::pair<float, float> getMousePos() { return Input::cursorPos; }

  float getOffset() {
    float o = scrollOffset;
    Input::scrollOffset = 0;
    return o;
  }

  static std::pair<int, bool> lastPressed;
  static std::pair<int, bool> lastMouseClick;
  static std::pair<float, float> cursorPos;
  static std::map<int, bool> keyMap;
  static float scrollOffset;
  static std::pair<bool, bool> mouseClick;
};
