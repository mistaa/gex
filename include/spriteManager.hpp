#pragma once

#include <GL/glew.h>
#include "renderSystem.hpp"
#include <map>
#include <memory>
#include <vector>

class spriteManager {
public:
  static unsigned int IDcount;
  using texSizes = std::pair<float, float>;
  using texGrids = std::pair<int, int>;

  std::map<unsigned int, std::pair<texSizes, texGrids>> textures;
  std::map<std::string, std::vector<std::shared_ptr<Sprite>>> spriteLayers;
  std::weak_ptr<Sprite> lastSprite;

  void addTexture(std::string filePath, int rows, int cols);
  void addSprite(glm::vec2 pos, std::string layer, float scale,
		 unsigned int texID, int texRow, int texCol, bool grid = false);
  void changeSpriteTex(std::weak_ptr<Sprite> sprite, unsigned int texID,
		       int texRow, int texCol);
  void exportSprites(std::string fileName);
  void deleteSprite(unsigned int tileID, std::string layer = "");
  void renderSprites(renderSystem &renderer, Shader &shader,
		     glm::vec2 pos);
  void cleanSprites();
  auto moveLastSprite() {
    std::shared_ptr<Sprite> ref = std::make_shared<Sprite>(*lastSprite.lock());
    deleteSprite(ref->ID, ref->layer);
    return ref;
  };
};
