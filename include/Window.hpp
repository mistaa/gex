#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <string>

void scrCallb(GLFWwindow *window, int width, int height);
void errCallb(int error, const char *desc);

class Window {
public:
  friend void scrCallb(GLFWwindow *window, int width, int height);

  Window(const int height, const int width, std::string title,
	 bool vSync = true);
  void init();
  void destroy() const;
  void update() const;
  auto getInstance() const { return _window; }
  bool closing() const { return glfwWindowShouldClose(_window); }
  static float _height, _width;

private:
  bool _vSync;
  GLFWwindow *_window;
  std::string _title;
};
