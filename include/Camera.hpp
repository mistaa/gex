#pragma once

#include "Entity.hpp"
#include <glm/glm.hpp>

class Camera : public Entity {
public:
  Camera(std::shared_ptr<Sprite> sprite, Shader &shader, float width, float height)
    : Entity(sprite, "camera"), shader(shader), scrWidth(width), scrHeight(height) {}
  // void processInput(Input &input);
  void update(animationSystem &aSystem, Input &inputSys) override;
  Shader &shader;
  glm::mat4 projection, view;
  glm::vec2 cameraPos2d;

private:
  float scrWidth, scrHeight;
};
