#pragma once

#include "spriteManager.hpp"
#include "Clock.hpp"

struct Animation {
  Animation(unsigned int textID, std::vector<std::pair<int, int>> tCoords,
	    unsigned int frames)
      : texID(textID), texCoords(tCoords), frames(frames) {
    currentTexCoord = texCoords.begin();
  }
  unsigned int texID;
  unsigned int frames;
  std::vector<std::pair<int, int>> texCoords;
  std::vector<std::pair<int, int>>::iterator currentTexCoord;
  Clock clock;
};

class animationSystem {
public:
  explicit animationSystem(spriteManager &sMan) : _sMan(sMan) {}
  spriteManager &_sMan;
  void playAnimation(std::weak_ptr<Sprite> sprite, float time,
		     std::string aName);
  void resetAnimation(std::weak_ptr<Sprite> sprite, std::string aName);
  void createAnimation(std::string name, unsigned int texID,
		       std::vector<std::pair<int, int>> texCoords);
  void createAnimation(std::string name, unsigned int texID, int row,
		       int startCol, int endCol);
  float getAnimationTime(std::string name, float time) {
    return (float)animations[name]->frames * time;
  }

private:
  std::map<std::string, std::shared_ptr<Animation>> animations;
};
