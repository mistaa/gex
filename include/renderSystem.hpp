#pragma once

#include <memory>
#include <GL/glew.h>
#include "Sprite.hpp"
#include "Shader.hpp"

class renderSystem {
public:
  renderSystem(float renderDistance) : renderDistance(renderDistance) {}
  void init();
  void render(std::weak_ptr<Sprite> sprite, Shader &shader, glm::vec2 pos) const;
  void clear();
  void destroy();


private:
  unsigned int VAO, EBO, VBO;
  float renderDistance;
};
