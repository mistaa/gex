#pragma once

#include <GLFW/glfw3.h>
#include <iostream>

struct Clock {

  Clock() {
    timer = 0;
    previousTime = 0;
    delta = 0;
    timeElapsed = 0;
    frames = 0;
  }

  float timer;
  float previousTime;
  float delta;
  float timeElapsed;
  int frames;

  inline void update() {
    double currentTime = glfwGetTime();
    delta = float(currentTime - previousTime);
    frames++;
    timeElapsed += delta;
    if (timeElapsed >= 1.0f) {
      std::cout << frames << "\n";
      std::cout << delta * 1000.0f << "\n";
      frames = 0;
      timeElapsed = 0;
    }
    previousTime = currentTime;
  }

  inline void start() { timer = glfwGetTime(); }
  inline float elapsed() { return float(glfwGetTime() - timer); }
};
