#pragma once

#include "Entity.hpp"
#include "renderSystem.hpp"

class entitySystem {
public:
  entitySystem(animationSystem &aSystem, Input &inputSys)
      : aSystem(aSystem), inputSys(inputSys) {}

  animationSystem &aSystem;
  Input &inputSys;

  std::vector<std::shared_ptr<Entity>> entities;

  template <typename T, typename... Args> void addEntity(Args &&... args) {
    entities.emplace_back(std::make_shared<T>(args...));
  }

  void updateEntities() {
    for (auto entity : entities) {
      for (auto childEnt : entity->childEntities) {
	childEnt->update(aSystem, inputSys);
      }
      entity->update(aSystem, inputSys);
    }
  }

  void renderEntities(renderSystem &renderer, Shader &shader) {
    for (auto entity : entities) {
      renderer.render(entity->sprite, shader, entity->sprite->position);
      for (auto childEnt : entity->childEntities) {
	renderer.render(childEnt->sprite, shader,
			childEnt->sprite->position);
      }
    }
  }

  void cleanEntities() {
    for (auto it = entities.begin(); it != entities.end();) {
      if ((*it)->inactive)
	entities.erase(it);
      else
	for (auto itx = (*it)->childEntities.begin();
	     itx != (*it)->childEntities.end();) {
	  if ((*itx)->inactive)
	    (*it)->childEntities.erase(itx);
	  else
	    itx++;
	}
      it++;
    }
  }

    template <typename T> std::shared_ptr<T> getEntity(std::string name) {
      for (auto entity : entities)
	if (entity->name == name)
	  return std::static_pointer_cast<T>(entity);
      return std::shared_ptr<T>();
    }
  };
