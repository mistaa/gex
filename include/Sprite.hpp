#pragma once

#include <array>
#include <glm/glm.hpp>
#include <utility>
#include <string>

struct Sprite {
  Sprite(std::string layer, glm::vec2 pos, float scale, unsigned int texID, unsigned int ID)
    : layer(layer), position(pos), scale(scale), textureID(texID), ID(ID) {}
  std::string layer;
  std::array<float, 20> vertices;
  std::array<int, 6> indices = {0, 1, 2, 1, 3, 2};
  glm::vec2 position;
  glm::vec2 size;
  float scale;
  bool inactive = false;
  unsigned int textureID, ID;
};
