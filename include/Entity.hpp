#pragma once

#include "animationSystem.hpp"
#include "Input.hpp"
#include <memory>

class Entity {
public:
  Entity(std::shared_ptr<Sprite> sprite, std::string name)
      : sprite(sprite), name(name) {}
  bool inactive = false;
  std::string name;
  std::shared_ptr<Sprite> sprite;
  std::vector<std::shared_ptr<Entity>> childEntities;
  virtual void update(animationSystem &aSystem, Input &inputSys) = 0;

protected:
  void destroy() { inactive = true; }
};

class Spell : public Entity {
public:
  Spell(std::shared_ptr<Sprite> sprite, std::string name, glm::vec2 direction,
	glm::vec2 pos)
      : Entity(sprite, name), dir(direction) {
    clock.start();
    speed *= 5;
  }

  glm::vec2 dir;
  glm::vec2 speed = {dir.x, dir.y};
  Clock clock;

  void update(animationSystem &aSystem, Input &inputSys) override {
    if (clock.elapsed() > 1) {
      speed = {0, 0};
      animate(aSystem);
    }
    sprite->position += speed;
  }

  void animate(animationSystem &aSystem) {
    if (clock.elapsed() > 1 + aSystem.getAnimationTime(name, 0.05)) {
      aSystem.resetAnimation(sprite, name);
      destroy();
    } else {
      aSystem.playAnimation(sprite, 0.05, name);
    }
  }
};

class Player : public Entity {
public:
  Player(std::shared_ptr<Sprite> sprite) : Entity(sprite, "player") {}
  std::vector<std::shared_ptr<Spell>> knownSpells;
  std::string idleAnimation, activeAnimation;
  bool isMoving;
  float movementSpeed = 5.f;

  void addSpell(std::shared_ptr<Sprite> sprite, std::string name) {
    knownSpells.emplace_back(
	std::make_shared<Spell>(sprite, name, glm::vec2(0), glm::vec2(0)));
  }

  void update(animationSystem &aSystem, Input &inputSys) override {
    input(inputSys);
    animate(aSystem);
  }

  void animate(animationSystem &aSystem) {
    if (isMoving)
      aSystem.playAnimation(sprite, 0.1f, activeAnimation);
    else
      aSystem.playAnimation(sprite, 0.f, idleAnimation);
  }

  void input(Input &inputSys) {

    if (inputSys.getMouseClickOnce(GLFW_MOUSE_BUTTON_LEFT)) {
      auto ptr = knownSpells.back();

      glm::vec2 pos = {inputSys.getMousePos().first - 1920 / 2,
		       -(inputSys.getMousePos().second - 1080 / 2)};
      glm::vec2 dir = glm::normalize(pos);

      auto sprit = std::make_shared<Sprite>(*ptr->sprite);
      sprit->position = sprite->position;

      childEntities.push_back(
	  std::make_shared<Spell>(sprit, ptr->name, dir, glm::vec2(0.f)));
    }

    if (inputSys.getKey(GLFW_KEY_W)) {
      sprite->position.y += movementSpeed;
      isMoving = true;
      activeAnimation = "up walk";
      idleAnimation = "up idle";
      return;
    }
    if (inputSys.getKey(GLFW_KEY_S)) {
      sprite->position.y -= movementSpeed;
      isMoving = true;
      activeAnimation = "down walk";
      idleAnimation = "down idle";
      return;
    }
    if (inputSys.getKey(GLFW_KEY_D)) {
      sprite->position.x += movementSpeed;
      isMoving = true;
      activeAnimation = "right walk";
      idleAnimation = "right idle";
      return;
    }
    if (inputSys.getKey(GLFW_KEY_A)) {
      sprite->position.x -= movementSpeed;
      isMoving = true;
      activeAnimation = "left walk";
      idleAnimation = "left idle";
      return;
    }
    if (inputSys.getKey(GLFW_KEY_Q)) {
      destroy();
    }
    isMoving = false;
  }
};
