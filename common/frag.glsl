#version 330 core

out vec4 fragColor;

uniform sampler2D textureid;

in vec2 tex;

void main() {
fragColor = texture(textureid, tex);
}
