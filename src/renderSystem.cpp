#include "../include/renderSystem.hpp"
#include <glm/ext/matrix_transform.hpp>

void renderSystem::init() {
  glGenVertexArrays(1, &VAO);
  glGenBuffers(1, &VBO);
  glGenBuffers(1, &EBO);
  glBindVertexArray(VAO);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void renderSystem::render(std::weak_ptr<Sprite> sprite, Shader &shader, glm::vec2 pos) const {

  auto pSprite = sprite.lock();
  if(!pSprite)
    return;

  if(pSprite->position.x > pos.x + renderDistance)
    return;
  if(pSprite->position.x < pos.x - renderDistance)
    return;
  if(pSprite->position.y > pos.y + renderDistance)
    return;
  if(pSprite->position.y < pos.y - renderDistance)
    return;

  shader.use();
  auto translate =
    glm::translate(glm::mat4(1.0f), glm::vec3(pSprite->position, 0.f));
  auto matrix = glm::scale(translate, glm::vec3(pSprite->scale));

  shader.setMat4("model", matrix);
  shader.setInt("texture", pSprite->textureID);

  glBindTexture(GL_TEXTURE_2D, pSprite->textureID);

  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(float) * pSprite->vertices.size(),
	       &pSprite->vertices.front(), GL_STATIC_DRAW);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(float) * pSprite->indices.size(),
	       &pSprite->indices.front(), GL_STATIC_DRAW);

  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *)0);

  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float),
			(void *)(3 * sizeof(float)));

  glDrawElements(GL_TRIANGLES, sizeof(unsigned int) * pSprite->indices.size(),
		 GL_UNSIGNED_INT, 0);
}

void renderSystem::clear() {
  glClearColor(35.0f / 255.0f, 120.0f / 255.0f, 120.0f / 255.0f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void renderSystem::destroy() {
  glDeleteBuffers(1, &VBO);
  glDeleteBuffers(1, &EBO);
  glDeleteVertexArrays(1, &VAO);
}
