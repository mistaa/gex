#include "../include/Input.hpp"
#include <iostream>

std::map<int, bool> Input::keyMap;
std::pair<int, bool> Input::lastPressed;
std::pair<int, bool> Input::lastMouseClick;
std::pair<float, float> Input::cursorPos;
float Input::scrollOffset;
std::pair<bool, bool> Input::mouseClick;

void keyCallb(GLFWwindow *window, int key, int, int action, int mods) {
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, true);
  if (action == GLFW_PRESS) {
    Input::keyMap[key] = true;
    Input::lastPressed = {key, true};
  }
  if (action == GLFW_RELEASE)
    Input::keyMap[key] = false;
}

void scrollCallb(GLFWwindow *window, double xoffset, double yoffset) {
  Input::scrollOffset = yoffset;
}

void cursorPosCallb(GLFWwindow *window, double xpos, double ypos) {
  Input::cursorPos.first = xpos;
  Input::cursorPos.second = ypos;
}

void mouseClickCallb(GLFWwindow *window, int button, int action, int mods) {
  if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
    Input::mouseClick.first = true;
    Input::lastMouseClick = {GLFW_MOUSE_BUTTON_LEFT, true};
  }
  if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS) {
    Input::mouseClick.second = true;
    Input::lastMouseClick = {GLFW_MOUSE_BUTTON_RIGHT, true};
  }
  if (action == GLFW_RELEASE) {
    Input::mouseClick.first = false;
    Input::mouseClick.second = false;
    Input::lastMouseClick = {-1, false};
  }
}

bool Input::getKeyOnce(int key) {
  if (Input::lastPressed.first == key && Input::getKey(key)) {
    Input::lastPressed = {-1, false};
    return true;
  }
  return false;
}
