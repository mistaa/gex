#include "../include/Shader.hpp"
#include <fstream>
#include <iostream>

void Shader::create(std::string srcvertexShader, std::string srcfragShader) {

  std::ifstream vs(srcvertexShader);
  std::ifstream fs(srcfragShader);

  std::string vsl;
  std::string line = "";
  while (getline(vs, line))
    vsl.append(line + '\n');

  std::string fsl;
  line = "";
  while (getline(fs, line))
    fsl.append(line + '\n');

  const char *vsdata = vsl.c_str();
  const char *fsdata = fsl.c_str();

  int success;
  char log[512];

  auto vertexShader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertexShader, 1, &vsdata, NULL);
  glCompileShader(vertexShader);
  glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
  if (!success) {
    puts("vertex shader error");
    glGetShaderInfoLog(vertexShader, 512, NULL, log);
    puts(log);
  }

  auto fragShader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragShader, 1, &fsdata, NULL);
  glCompileShader(fragShader);
  glGetShaderiv(fragShader, GL_COMPILE_STATUS, &success);
  if (!success) {
    puts("fragment shader error");
    glGetShaderInfoLog(fragShader, 512, NULL, log);
    puts(log);
  }

  shaderProgram = glCreateProgram();
  glAttachShader(shaderProgram, vertexShader);
  glAttachShader(shaderProgram, fragShader);
  glLinkProgram(shaderProgram);
  glDeleteShader(vertexShader);
  glDeleteShader(fragShader);
  vs.close();
  fs.close();
}

void Shader::destroy() { glDeleteProgram(shaderProgram); }

void Shader::use() { glUseProgram(shaderProgram); }

void Shader::setMat4(const std::string &name, const glm::mat4 &mat) {
  glUniformMatrix4fv(glGetUniformLocation(shaderProgram, name.c_str()), 1,
		     GL_FALSE, &mat[0][0]);
}

void Shader::setInt(const std::string &name, const int num) {
  glUniform1i(glGetUniformLocation(shaderProgram, name.c_str()), num);
}
