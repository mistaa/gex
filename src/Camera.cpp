#include "../include/Camera.hpp"

#include <glm/ext/matrix_clip_space.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>

void Camera::update(animationSystem &aSystem, Input &inputSys) {

  cameraPos2d = {sprite->position.x + sprite->size.x / 2.f,
		 sprite->position.y + sprite->size.y / 2.f};
  shader.use();
  shader.setMat4("projection", projection);
  shader.setMat4("view", view);

  float cameraWidth = scrWidth / 2.f;
  float cameraHeight = scrHeight / 2.f;

  projection =
      glm::ortho(-cameraWidth, cameraWidth, -cameraHeight, cameraHeight);

  // first x and y to offset correctly camera
  glm::vec3 cameraPos = {cameraPos2d, 0.f};

  glm::vec3 cameraFront = {0.0f, 0.0f, -1.0f};
  view = glm::lookAt(cameraPos, cameraPos + cameraFront, glm::vec3(0, 1, 0));
}
