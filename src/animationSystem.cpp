#include "../include/animationSystem.hpp"
#include <iostream>
#include <utility>
#include <vector>

void animationSystem::playAnimation(std::weak_ptr<Sprite> sprite, float time,
				    std::string aName) {

  auto pSprite = sprite.lock();
  if (!pSprite || animations.find(aName) == animations.end())
    return;

  auto el = animations[aName];
  if (el->clock.elapsed() > time) {
    _sMan.changeSpriteTex(pSprite, el->texID, el->currentTexCoord->first,
			  el->currentTexCoord->second);
    el->currentTexCoord++;
    if (el->currentTexCoord == el->texCoords.end())
      el->currentTexCoord = el->texCoords.begin();
    el->clock.start();
  }
}

void animationSystem::resetAnimation(std::weak_ptr<Sprite> sprite, std::string aName) {
  auto el = animations[aName];
  auto pSprite = sprite.lock();
  if(!pSprite)
    return;
  _sMan.changeSpriteTex(pSprite, el->texID, el->texCoords.begin()->first,
			  el->texCoords.begin()->second);
  el->currentTexCoord = el->texCoords.begin();
}

void animationSystem::createAnimation(
    std::string name, unsigned int texID,
    std::vector<std::pair<int, int>> texCoords) {

  if (_sMan.textures.find(texID) == _sMan.textures.end()) {
    std::cout << "texture " << texID << " not found" << std::endl;
    return;
  }
  animations[name] = std::make_shared<Animation>(texID, texCoords, texCoords.size());
}

void animationSystem::createAnimation(std::string name, unsigned int texID,
				      int row, int startCol, int endCol) {

  std::vector<std::pair<int, int>> texCoords;

  for (int i = startCol; i < endCol; i++)
    texCoords.emplace_back(i, row);

  animations[name] = std::make_shared<Animation>(texID, texCoords, endCol - startCol);
}
