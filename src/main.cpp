#include "../include/Gex.hpp"

int main(int argc, char *argv[]) {

  Window window(1920.f, 1080.f, "test");
  window.init();
  Clock clock;

  renderSystem renderer(2000.0f);
  renderer.init();

  Shader basic;
  basic.create("common/vert.glsl", "common/frag.glsl");

  Input input;
  input.setCallback(window.getInstance());

  spriteManager sMan;
  sMan.addTexture("./common/zombie-male-base.png", 4, 3);
  sMan.addTexture("./common/explosion-4.png", 1, 12);
  sMan.addTexture("./common/terrain.png", 6, 8);
  sMan.addTexture("./common/dungeon-tileset.png", 16, 16);
  sMan.addTexture("./common/explosion-6.png", 1, 8);

  animationSystem aSys(sMan);
  aSys.createAnimation("up walk", 1, {{0, 0}, {2, 0}});
  aSys.createAnimation("up idle", 1, {{1, 0}});
  aSys.createAnimation("down walk", 1, {{0, 3}, {2, 3}});
  aSys.createAnimation("down idle", 1, {{1, 3}});
  aSys.createAnimation("left walk", 1, {{0, 2}, {2, 2}});
  aSys.createAnimation("left idle", 1, {{1, 2}});
  aSys.createAnimation("right walk", 1, {{0, 1}, {2, 1}});
  aSys.createAnimation("right idle", 1, {{1, 1}});
  aSys.createAnimation("explosion", 2, 0, 0, 12);
  aSys.createAnimation("explosion2", 5, 0, 0, 8);

  for (int i = 0; i < 100; ++i)
    for (int j = 0; j < 100; ++j)
      sMan.addSprite({i, j}, "1-back", 3, 3, 0, 0, true);

  for (int i = 0; i < 16; ++i)
    for (int j = 0; j < 16; ++j)
      sMan.addSprite({i, j}, "2-front", 3, 4, i, j, true);

  entitySystem entSys(aSys, input);

  sMan.addSprite({0, 0}, "2-front", 2, 1, 0, 0);
  entSys.addEntity<Player>(sMan.moveLastSprite());

  sMan.addSprite({0, 0}, "2-front", 1, 2, 0, 0);
  entSys.getEntity<Player>("player")->addSpell(sMan.moveLastSprite(), "explosion");

  // sMan.exportSprites("test.txt");

  entSys.addEntity<Camera>(entSys.getEntity<Player>("player")->sprite, basic,
  			   Window::_width, Window::_height);

  while (!window.closing()) {
    renderer.clear();
    entSys.updateEntities();
    entSys.cleanEntities();
    sMan.renderSprites(renderer, basic,
    		       entSys.getEntity<Camera>("camera")->cameraPos2d);
    entSys.renderEntities(renderer, basic);
    window.update();
  }
  basic.destroy();
  window.destroy();
}
