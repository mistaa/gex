#include "../include/Window.hpp"
#include <iostream>

float Window::_height, Window::_width;

void scrCallb(GLFWwindow *window, int width, int height) {
  Window::_width = width;
  Window::_height = height;
  glViewport(0, 0, Window::_width, Window::_height);
}

void errCallb(int error, const char *desc) {
  std::cout << error << " : " << desc << "\n";
}

Window::Window(const int width, const int height, std::string title,
	       bool vSync) {
  _width = width;
  _height = height;
  _title = title;
  _vSync = vSync;
  _window = nullptr;
}

void Window::init() {
  glfwSetErrorCallback(errCallb);
  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  _window = glfwCreateWindow(_width, _height, _title.c_str(),
			     glfwGetPrimaryMonitor(), 0);
  glfwMakeContextCurrent(_window);
  glfwSetFramebufferSizeCallback(_window, scrCallb);
  // glfwSetInputMode(_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
  glfwSwapInterval(_vSync);
  glewInit();
}

void Window::destroy() const {
  glfwDestroyWindow(_window);
  glfwTerminate();
}

void Window::update() const {
  glfwSwapBuffers(_window);
  glfwPollEvents();
}
