#define STB_IMAGE_IMPLEMENTATION
#include "../include/spriteManager.hpp"
#include "../include/stb_image.h"
#include <GL/glew.h>
#include <fstream>
#include <iostream>
#include <ostream>

unsigned int spriteManager::IDcount = 0;

void spriteManager::addTexture(std::string filePath, int rows, int cols) {
  stbi_set_flip_vertically_on_load(true);
  unsigned int texture;
  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  int width, height, nrChannels;
  unsigned char *data =
      stbi_load(filePath.c_str(), &width, &height, &nrChannels, 0);
  if (data) {
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA,
		 GL_UNSIGNED_BYTE, data);
    textures[texture] = {{width, height}, {cols, rows}};
  } else {
    std::cout << "Failed to load texture" << std::endl;
  }
  stbi_image_free(data);
}

void spriteManager::addSprite(glm::vec2 pos, std::string layer, float scale,
			      unsigned int texID, int texRow, int texCol,
			      bool grid) {
  if (textures.find(texID) == textures.end()) {
    std::cout << "texture " << texID << " not found" << std::endl;
    return;
  }

  auto texSizes = textures[texID].first;
  auto texGrids = textures[texID].second;

  float rowSize = texSizes.first / texGrids.first;
  float colSize = texSizes.second / texGrids.second;

  float rowTexSize = 1.f / texGrids.first;
  float colTexSize = 1.f / texGrids.second;

  if (grid)
    spriteLayers[layer].emplace_back(std::make_shared<Sprite>(
	layer, glm::vec2(pos.x * rowSize * scale, pos.y * colSize * scale),
	scale, texID, IDcount++));
  else
    spriteLayers[layer].emplace_back(
	std::make_shared<Sprite>(layer, pos * scale, scale, texID, IDcount++));

  lastSprite = spriteLayers[layer].back();

  auto pSprite = lastSprite.lock();
  if (!pSprite)
    return;

  pSprite->size.x = rowSize * scale;
  pSprite->size.y = colSize * scale;

  for (int i = 0, vertPos = 0; i < 2; i++) {
    for (int j = 0; j < 2; j++, vertPos += 5) {
      pSprite->vertices[vertPos] = rowSize * j;
      pSprite->vertices[vertPos + 1] = colSize * i;
      pSprite->vertices[vertPos + 2] = 0.f;
      pSprite->vertices[vertPos + 3] = rowTexSize * (j + texRow);
      pSprite->vertices[vertPos + 4] = colTexSize * (i + texCol);
    }
  }
}

void spriteManager::changeSpriteTex(std::weak_ptr<Sprite> sprite,
				    unsigned int texID, int texRow,
				    int texCol) {

  if (textures.find(texID) == textures.end()) {
    std::cout << "texture " << texID << " not found" << std::endl;
    return;
  }

  auto pSprite = sprite.lock();
  if (!pSprite)
    return;

  pSprite->textureID = texID;

  auto texSizes = textures[texID].first;
  auto texGrids = textures[texID].second;

  float rowSize = 1.f / texGrids.first;
  float colSize = 1.f / texGrids.second;

  for (int i = 0, vertPos = 3; i < 2; i++) {
    for (int j = 0; j < 2; j++, vertPos += 5) {
      pSprite->vertices[vertPos] = rowSize * (j + texRow);
      pSprite->vertices[vertPos + 1] = colSize * (i + texCol);
    }
  }
}

void spriteManager::exportSprites(std::string fileName) {
  std::ofstream output(fileName);
  for (const auto layer : spriteLayers) {
    for (const auto sprite : layer.second) {
      output << sprite->position[0] << " ";
      output << sprite->position[1] << " ";
      output << sprite->scale << " ";
      output << sprite->layer << " ";
      for (const auto vert : sprite->vertices)
	output << vert << " ";
      for (const auto ind : sprite->indices)
	output << ind << " ";
      output << sprite->textureID << " ";
    }
  }
}

void spriteManager::deleteSprite(unsigned int spriteID, std::string layer) {
  if (layer == "") {
    for (auto &layer : spriteLayers) {
      for (auto sprite : layer.second) {
	if (sprite->ID == spriteID) {
	  sprite->inactive = true;
	  IDcount--;
	}
      }
    }
  } else {
    for (auto sprite : spriteLayers[layer]) {
      if (sprite->ID == spriteID) {
	sprite->inactive = true;
	IDcount--;
      }
    }
  }
}

void spriteManager::renderSprites(renderSystem &renderer, Shader &shader,
				  glm::vec2 pos) {
  for (auto &layer : spriteLayers) {
    for (auto sprite : layer.second) {
      if(!sprite->inactive)
      renderer.render(sprite, shader, pos);
    }
  }
}

void spriteManager::cleanSprites() {
  for (auto &layer : spriteLayers) {
    for (auto it = layer.second.begin(); it != layer.second.end();) {
      if ((*it)->inactive) {
        layer.second.erase(it);
        IDcount--;
      } else {
        ++it;
      }
    }
  }
}
